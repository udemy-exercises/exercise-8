using System;

class exercise8
{
   static void Main()
   {
      int[] array;
      int arrayLength = int.Parse(Console.ReadLine());
      array = new int[arrayLength];

      int evenAmount = 0;
      int[] evenNums = new int[arrayLength];
      for (int i = 0; i < arrayLength; i++) {evenNums[i] = 1;}

      for (int i = 0; i < arrayLength; i++)
      {
         array[i] = int.Parse(Console.ReadLine());

         if (array[i] % 2 == 0) {
            evenNums[i] = array[i];
            evenAmount++;
         }
      }

      Console.WriteLine();
      Console.WriteLine("Even Numbers:");
      foreach (int even in evenNums)
      {
         if (even != 1) {
            Console.WriteLine(even);
         }
      }

      Console.WriteLine("Amount of even numbers");
      Console.WriteLine(evenAmount);
   }
}
