using System;

class exercise8
{
   static void Main()
   {
      int N = int.Parse(Console.ReadLine());

      double[] array;
      array = new double[N];

      for (int i = 0; i < N; i++)
      {
         array[i] = double.Parse(Console.ReadLine());
      }

      double X = array[0];
      int pos = 0;
      for (int i = 0; i < N; i++)
      {
         if (X < array[i]) {
            X = array[i];
            pos = i;
         }
      }
      
      Console.WriteLine(X);
      Console.WriteLine(pos);
   }
}
