using System;

class exercise8
{
   static void Main()
   {
      int[] A;
      int[] B;
      int[] C;

      int N = int.Parse(Console.ReadLine());

      A = new int[N];
      B = new int[N];
      C = new int[N];

      Console.WriteLine();
      for (int i = 0; i < N; i++)
      {
         A[i] = int.Parse(Console.ReadLine());
      }

      Console.WriteLine();
      for (int i = 0; i < N; i++)
      {
         B[i] = int.Parse(Console.ReadLine());
      }

      Console.WriteLine();
      for (int i = 0; i < N; i++)
      {
         C[i] = A[i] + B[i];
         Console.WriteLine(C[i]);
      }
   }
}
